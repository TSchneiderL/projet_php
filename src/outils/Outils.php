<?php

namespace projet_php\outils;

class Outils{
	
	
	public static function headerHTML($titre, $leCSS){
		echo 
		"<!DOCTYPE html>
		<html>
			<head>
				<meta charset=\"utf-8\" />
				<link rel='stylesheet' href='css/$leCSS' />
				
				<title>$titre</title>
			";
			
	}
	
	public static function footerHTML(){
		echo"
			</body>
		</html>";
	}
	
	
}
?>