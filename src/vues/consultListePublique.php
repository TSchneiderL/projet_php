<?php
namespace projet_php\vues;

use projet_php\modele\Liste;
use projet_php\modele\Item;

class consultListePublique {

	public static function detailListePub() {
		
	$idDeListe = $_GET['idListe'];
	
	//Requete qui selectionne la liste selon son token public
	$maliste = Liste::select('*')
						->where('tokenConsult', '=', $idDeListe)
						->first();
	
//Et affiche ses items	
	$itemsliste = Item::where('liste_id','=',$maliste->no)
					->get();
					
		

		if (isset( $_SESSION[ 'idSess' ])) 
		{
			
		?>
	
		</head>
			
			<body>
				<div class="page">
				
					<ul class="menu">
						<li>
							<a href="index" title="Page d'accueil">Home</a>
						</li>
						<li>
							<a href="new_liste" title="Créer une liste">Créer une liste</a>
						</li>	
						<li>
							<a class="pageActive" href="mesListes" title="Mes listes">Mes listes</a>
						</li>
						<li>
							<a href="." title="Voir les créateurs publics">Créateurs de listes</a>
						</li>
						<li>
							<a href="." title="Mon Compte">Mon Compte</a>
						</li>
						<li>
							<a href="logout" title="Deconnexion">Deconnexion</a>
						</li>
						<li>
							<a href="." title="Listes publiques">Listes publiques</a>
						</li>
					</ul>
		<?php
			
		}
		else
		{
		?>
			</head>
			
			<body>
				<div class="page">
				
					<ul class="menu">
					
						<li class="activPage">
							<a href="index" title="Page d'accueil">Home</a>
						</li>
						<li>
							<a href="login" title="Se connecter">Se connecter</a>
						</li>	
						<li>
							<a href="register" title="S'inscrire">S'inscrire</a>
						</li>
						<li>
							<a href="new_liste" title="Créer une liste">Créer une liste</a>
						</li>
					</ul>
					
			</div>
		<?php
		}
		echo 
			'<table>
				<caption>'.$maliste->titre . ' (date expiration: ' . $maliste->expiration . ') :</caption>
				<tr><td>'.$maliste->description.'</td></tr>

				<tr>
					<th>Logo items</th>
					<th>Nom items</th>
					<th>Tarif items</th>
					<th>Reservation items</th>
				</tr>';
			foreach ($itemsliste as $item) {			
		
				//Vérification si le lien d'une image donnée est l'URL complete
				//ou bien un chemin relatif.
				$lienHTTP = substr($item->url, 0, 4);
				$lienHTTPS = substr($item->url, 0, 5);
				
				echo
				"<tr>";
					if($lienHTTP == 'http' || $lienHTTPS == 'https') 
					{
						echo "<td><img src='$item->url' width=100px/></td>";
					}
					else
					{
						echo "<td><img src='$item->url'/></td>";
					}
					echo "<td>$item->nom</td>
						<td>$item->tarif</td>";
					if($item->estreserve == 0){
						echo "<td>non</td>
								<td><a href='choisirItem?idItem=$item->id&idListe=$idDeListe'>Reserver</a></td>";
							
					}else{
						echo "<td>oui</td>";
					}
				echo "</tr>";
				
				
			}
			?>
			</table><br>
			
			<div class="button">
                <input type="button" value="Accueil" onclick="location='index'">
            </div>
			<?php
	}
}

?>