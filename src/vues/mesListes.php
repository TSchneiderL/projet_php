<?php
namespace projet_php\vues;

use projet_php\modele\Liste;
use projet_php\modele\Item;

class mesListes {

	public static function voirListes() {

		//Voir les listes de l'utilisateur connecté
		$listes = Liste::select('*')
					->where('user_id', '=', $_SESSION[ 'idSess' ])
					->get();
		?>

		</head>
			
			<body>
				<div class="page">
				
					<ul class="menu">
						<li>
							<a href="index" title="Page d'accueil">Home</a>
						</li>
						<li>
							<a href="new_liste" title="Créer une liste">Créer une liste</a>
						</li>	
						<li>
							<a class="pageActive" href="mesListes" title="Mes listes">Mes listes</a>
						</li>
						<li>
							<a href="." title="Voir les créateurs publics">Créateurs de listes</a>
						</li>
						<li>
							<a href="." title="Mon Compte">Mon Compte</a>
						</li>
						<li>
							<a href="logout" title="Deconnexion">Deconnexion</a>
						</li>
						<li>
							<a href="." title="Listes publiques">Listes publiques</a>
						</li>
					</ul>
		<?php
			
			echo 
			'<table>
				<tr>
					<th>Nom Liste</th>
					<th>Date Expiration</th>
					<th>Modification</th>
					<th>Suppression</th>
					<th>Consultation</th>
				</tr>';
			foreach ($listes as $liste) {
				
				echo
				"<tr>
					<td>$liste->titre</td>
					<td>$liste->expiration</td>
					<td><a href='modifListe?idListe=$liste->tokenModif'>Modifier</a>
					<td><a href='deleteList?idListe=$liste->tokenModif'>Supprimer</a></td>
					<td><a href='consulListe?idListe=$liste->tokenModif'>Consulter</a></td>
					<td><a href='partageListe?idListe=$liste->tokenConsult'>Partager</a></td>
				</tr>";
			}
			echo
			'</table><br>
			
		';
		
	}
}

?>