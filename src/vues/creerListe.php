<?php
namespace projet_php\vues;

class creerListe {

	public static function addListe() {

if (isset( $_SESSION[ 'idSess' ])) 
{

?>

</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
				<li>
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>	
				<li>
					<a href="mesListes" title="Mes listes">Mes listes</a>
				</li>
				<li>
					<a href="." title="Voir les créateurs publics">Créateurs de listes</a>
				</li>
				<li>
					<a href="." title="Mon Compte">Mon Compte</a>
				</li>
				<li>
					<a href="logout" title="Deconnexion">Deconnexion</a>
				</li>
				<li>
					<a href="." title="Listes publiques">Listes publiques</a>
				</li>
			</ul>
<?php
}
else
{
?>
	</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
			
				<li class="activPage">
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a href="login" title="Se connecter">Se connecter</a>
				</li>	
				<li>
					<a href="register" title="S'inscrire">S'inscrire</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>
			</ul>
<?php
}
?>

		<form action="newListe" method="post"> 
			<div class="divTitre">
				<label id="titleForm">Nouvelle liste</label>
			</div>
			
			<div>
				<label class="titre">Titre de ma liste:</label>
				<input type="text" name="titreListe" placeholder="Anniversaire Allan !!" required="required"/>
			</div>
			<div>
				<label class="description">Description:</label>
				<textarea name="descListe" placeholder="On fête les 22 ans d'Allan..." required="required"></textarea>
			</div>
			<div> 
				<label class="date">Date d'expiration:</label>
				<input type="date" name="dateExpir" required="required"/>
			</div>
  
			<div class="button">
				<button type="submit">Créer ma liste</button>
			</div>
		</form>
		
		</div>
<?php
}
}

?>