<?php
namespace projet_php\vues;

use projet_php\modele\Item;
use projet_php\modele\Liste;

class supprimerListe {
	
	public static function delListe($idListe) {
		
		
			//Selectionne la liste choisi pour la supprimer
		$listeId = Liste::where( 'tokenModif', '=', $idListe)
				->first();
		
			//Selectionne les items de cette liste choisie pour les supprimer aussi	
		$itemsId = Item::where( 'liste_id', '=', $listeId->no)
				->get();
		
		foreach($itemsId as $lesItems)
		{
			$lesItems->delete();
		}
		
		$listeId->delete();
		
		if(isset($_SESSION['idSess']))
		{
			echo '<meta http-equiv="refresh" content="0; URL=mesListes">';

		}
		else
		{
			echo '<meta http-equiv="refresh" content="0; URL=index">';

		}
		echo '<body onload="alert(\'Votre liste a bien été supprimée.\')">';
	}
}

?>