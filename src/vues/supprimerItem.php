<?php
namespace projet_php\vues;

use projet_php\modele\Item;
use projet_php\modele\Liste;

class supprimerItem {
	
	public static function delItem($idItem) {
		
		$items = Item::get();
		
			//Selectionne l'item choisi pour le supprimer	
		$itemId = Item::where( 'id', '=', $idItem)
				->first();
		
		$itemId->delete();
		
		if(isset($_SESSION['idSess']))
		{
			echo '<meta http-equiv="refresh" content="0; URL=mesListes">';

		}
		else
		{
			echo '<meta http-equiv="refresh" content="0; URL=index">';

		}
		echo '<body onload="alert(\'Votre item a bien été supprimé.\')">';
	}
}

?>