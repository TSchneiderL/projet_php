<?php
namespace projet_php\vues;

use projet_php\modele\Utilisateur;
use projet_php\modele\Liste;
use projet_php\modele\Item;

class modifListe {

	public static function modifierListe($tokenModifListe) {

	//Requete qui selectionne la liste en récupérant son token de modification
	$getListe = Liste::select( '*')
				->where('tokenModif', '=', $tokenModifListe)
				->first();
	
	//selectionne les items de l'id de la liste
	$getNbItem = Item::select('id')
				->where('liste_id', '=', $getListe->no)
				->get();
				
	//Compteur pour savoir combien d'items sont présent dans cette liste
	$cmptItem = 0;
	
	foreach($getNbItem as $nbItem)
	{
		$cmptItem++;
	}
				
if (isset( $_SESSION[ 'idSess' ])) 
{
	
	$letokenConsult = $_GET['idListe'];

?>

</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
				<li>
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>	
				<li>
					<a href="mesListes" title="Mes listes">Mes listes</a>
				</li>
				<li>
					<a href="." title="Voir les créateurs publics">Créateurs de listes</a>
				</li>
				<li>
					<a href="." title="Mon Compte">Mon Compte</a>
				</li>
				<li>
					<a href="logout" title="Deconnexion">Deconnexion</a>
				</li>
				<li>
					<a href="." title="Listes publiques">Listes publiques</a>
				</li>
			</ul>
<?php
}
else
{
?>
	</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
			
				<li class="activPage">
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a href="login" title="Se connecter">Se connecter</a>
				</li>	
				<li>
					<a href="register" title="S'inscrire">S'inscrire</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>
			</ul>
<?php
}
?>

		<form action="modListe?idListe=<?php echo $tokenModifListe ?>" method="post"> 
			<div class="divTitre">
				<label id="titleForm">Modifier ma liste</label>
			</div>
			
			<div>
				<label class="titre">Titre de ma liste:</label>
				<input type="text" name="titreListe" placeholder="Anniversaire Allan !!" required="required" value= <?php echo '"'.$getListe->titre.'"' ?>/>
			</div>
			<div>
				<label class="description">Description:</label>
				<textarea name="descListe" placeholder="On fête les 22 ans d'Allan..." required="required"><?php echo $getListe->description ?></textarea>
			</div>
			<div> 
				<label class="date">Date d'expiration:</label>
				<input type="date" name="dateExpir" required="required" value= <?php echo '"'.$getListe->expiration.'"' ?>/>
			</div>
			
			<div class="button">
				<button type="submit">Mettre à jour ma liste</button>
			</div>
		</form>
		
		
		
		
		<form action="addItem?idListe=<?php echo $tokenModifListe ?>" method="post"> 
			<div class="divTitre">
				<label id="titleForm">Ajout d'items</label>
			</div>
			
			<div> 
				<label class="titreItem">Mon item:</label>
				<input type="text" name="nomItem"/>
			</div>
			<div> 
				<label class="descriptionItel">Description de mon item:</label>
				<textarea name="descItem"/> </textarea>
			</div>
			<div> 
				<label class="lienItem">Lien vers l'item:</label>
				<input type="text" name="urlItem" placeholder="http://bouteilles_de_champagnes.com"/>
			</div>
			<div> 
				<label class="prixItem">Le tarif:</label>
				<input type="text" name="tarifItem" placeholder="48.90"/>
			</div>
			
			<div class="divTitre">
				<label id="titleForm">Il y a déjà <?php echo $cmptItem; ?> item(s) dans cette liste.</label>
			</div>
  
			
			<div class="button">
				<button type="submit">Ajouter cet item</button>
			</div>
	
		</form>
		
			<div class="button">
                <input  type="button" value="Retour" onclick="location='mesListes'">
            </div>
		</div>
<?php
}
}

?>