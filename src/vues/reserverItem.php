<?php
namespace projet_php\vues;

use projet_php\modele\Liste;
use projet_php\modele\Item;

class reserverItem {

	public static function prendreItem($idItem, $idListe) {

	$getItem = Item::select( '*')
				->where('id', '=', $idItem)
				->first();

				
if (isset( $_SESSION[ 'idSess' ])) 
{


?>

</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
				<li>
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>	
				<li>
					<a href="mesListes" title="Mes listes">Mes listes</a>
				</li>
				<li>
					<a href="afficherCreateurs.php" title="Voir les créateurs publics">Créateurs de listes</a>
				</li>
				<li>
					<a href="monCompte.php" title="Mon Compte">Mon Compte</a>
				</li>
				<li>
					<a href="logout" title="Deconnexion">Deconnexion</a>
				</li>
				<li>
					<a href="listesPubliques.php" title="Listes publiques">Listes publiques</a>
				</li>
			</ul>
<?php
}
else
{
?>
	</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
			
				<li class="activPage">
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a href="login" title="Se connecter">Se connecter</a>
				</li>	
				<li>
					<a href="register" title="S'inscrire">S'inscrire</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>
			</ul>
<?php
}
?>		
		
		<form action="faireResa?idListe=<?php echo $idListe ?>&idItem=<?php echo $idItem ?>" method="post"> 
			<div class="divTitre">
				<label id="titleForm">Reserver <?php echo $getItem->nom; ?></label>
			</div>
			
			<div class="divDescription">
				<label id="descriptionForm">Veuillez entrez votre nom et un message si vous le souhaitez :</label>
			</div>
			
			<div> 
				<label class="nomUserResa">Nom:</label>
				<input type="text" name="nomResa" required="required"/>
			</div>
			<div> 
				<label class="messageReservation">Message au créateur:</label>
				<textarea name="msgResa"/></textarea>
			</div>
  	
			<div class="button">
				<button type="submit">Reserver</button>
			</div>
	
		</form>
		
			<div class="button">
                <input type="button" value="Retour" onclick="location='vueListe?idListe=<?php echo $idListe ?>'">
            </div>
		</div>
<?php
}
}

?>