<?php
namespace projet_php\vues;

class login {

	public static function logUser() {

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=\"utf-8\" />
		<link rel="stylesheet" href="css/login.css" />
		<title>Login</title>
	</head>

	<body>
		
		<div class="page">
		
			<ul class="menu">
				<li>
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a class="pageActive" href="login" title="Se connecter">Se connecter</a>
				</li>	
				<li>
					<a href="register" title="S'inscrire">S'inscrire</a>
				</li>
				<li>
					<a href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>
			</ul>

		<form action="cnxUser" method="post"> 
		<div class="titreCnx">
			<label for="title">Connexion</label>
		</div>
		
		<div>
			<label class="name">Pseudo:</label>
			<input type="text" name="pseudo" placeholder="dupont54" required="required"/>
		</div>
		<div>
			<label class="mail">Mot de passe:</label>
			<input type="password" name="mdp"  required="required"/>
		</div>
  
	<div class="button">
        <button type="submit">Se connecter</button>
    </div>
	
	</form>
	</div>
</body>	
	
</html>
<?php
}
}
?>