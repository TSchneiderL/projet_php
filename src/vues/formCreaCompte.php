<?php

namespace projet_php\vues;

class formCreaCompte {

	public static function creaCompte() {

?>

<html>
	<head>
		<title>Inscription</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/inscription.css" />
	</head>
	
	<body>
	
	<div class="page">
		
			<ul class="menu">
				<li>
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a href="login" title="Se connecter">Se connecter</a>
				</li>	
				<li>
					<a class="pageActive" href="register" title="S'inscrire">S'inscrire</a>
				</li>
				<li>
					<a href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>
			</ul>

	<form name="inscr_user" action="addUser" method="post"> 
		<div class="titreCnx">
			<label for="title">Inscription</label>
		</div>
		
		<div>
			<label class="name">Pseudo:</label>
			<input type="text" name="pseudo" required="required"/>
		</div>
		
		<div>
			<label class="mdp">Mot de passe:</label>
			<input type="password" name="mdp" required="required"/>
		</div>
		
		<div>
			<label class="mdp">Confirmez votre mot de passe:</label>
			<input type="password" name="mdp2" required="required"/>
		</div>
  
	<div class="button">
        <button type="submit">S'inscrire</button>
    </div>
	
	</form>
	</div>

</body>
	
</html>
<?php
	}
}

?>