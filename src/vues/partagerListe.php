<?php
namespace projet_php\vues;

use projet_php\modele\Liste;

class partagerListe {
	
	public static function urlPartage($tokenConsultListe) {
		
		$liste = Liste::get();

		$getUrlConsultListe = Liste::select( '*')
				->where('tokenConsult', '=', $tokenConsultListe)
				->first();
		
		echo '<body onload="alert(\'Voici le lien de la liste à partager: \n\nprojet_php/vueListe?idListe='.$getUrlConsultListe->tokenConsult.'\n\nToute personne possedant ce lien pourra accéder aux informations \nde la liste et réserver un item, mais sans pouvoir la modifier. \')">';
		echo '<meta http-equiv="refresh" content="0; URL=mesListes">';
		
	}
	
}
?>
