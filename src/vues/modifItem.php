<?php
namespace projet_php\vues;

use projet_php\modele\Liste;
use projet_php\modele\Item;

class modifItem {

	public static function modifierItem($tokenModifListe, $idItem) {

	$getItem = Item::select( '*')
				->where('id', '=', $idItem)
				->first();

				
if (isset( $_SESSION[ 'idSess' ])) 
{


?>

</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
				<li>
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>	
				<li>
					<a href="mesListes" title="Mes listes">Mes listes</a>
				</li>
				<li>
					<a href="." title="Voir les créateurs publics">Créateurs de listes</a>
				</li>
				<li>
					<a href="." title="Mon Compte">Mon Compte</a>
				</li>
				<li>
					<a href="logout" title="Deconnexion">Deconnexion</a>
				</li>
				<li>
					<a href="." title="Listes publiques">Listes publiques</a>
				</li>
			</ul>
<?php
}
else
{
?>
	</head>
	
	<body>
		<div class="page">
		
			<ul class="menu">
			
				<li class="activPage">
					<a href="index" title="Page d'accueil">Home</a>
				</li>
				<li>
					<a href="login" title="Se connecter">Se connecter</a>
				</li>	
				<li>
					<a href="register" title="S'inscrire">S'inscrire</a>
				</li>
				<li>
					<a class="pageActive" href="new_liste" title="Créer une liste">Créer une liste</a>
				</li>
			</ul>
<?php
}
?>		
		
		<form action="changeItem?idListe=<?php echo $tokenModifListe ?>&idItem=<?php echo $idItem ?>" method="post"> 
			<div class="divTitre">
				<label id="titleForm">Modifier mon item</label>
			</div>
			
			<div> 
				<label class="titreItem">Mon item:</label>
				<input type="text" name="nomItem" value= <?php echo '"'.$getItem->nom.'"' ?>/>
			</div>
			<div> 
				<label class="descriptionItel">Description de mon item:</label>
				<textarea name="descItem"/><?php echo $getItem->descr ?></textarea>
			</div>
			<div> 
				<label class="lienItem">Lien vers l'item:</label>
				<input type="text" name="urlItem" value= <?php echo '"'.$getItem->url.'"' ?>/>
			</div>
			<div> 
				<label class="prixItem">Le tarif:</label>
				<input type="text" name="tarifItem" value= <?php echo '"'.$getItem->tarif.'"' ?>/>
			</div>
  
			
			<div class="button">
				<button type="submit">Modifier cet item</button>
			</div>
	
		</form>
		
			<div class="button">
                <input  type="button" value="Retour" onclick="location='consulListe?idListe=<?php echo $tokenModifListe ?>'">
            </div>
		</div>
<?php
}
}

?>