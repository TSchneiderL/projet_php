<?php
	namespace projet_php\modele;
	
	class Liste extends
		\Illuminate\Database\Eloquent\Model{
			protected $table = 'liste';
			protected $primaryKey = 'no';
			public $timestamps = false;
		}
?>