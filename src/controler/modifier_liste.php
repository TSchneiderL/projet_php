<?php
namespace projet_php\controler;

use projet_php\modele\Liste;
use projet_php\modele\Item;

class modifier_liste {
	
	public static function majListe($tokenModifListe) {

		$listes = Liste::get();
		

		if ($_POST["dateExpir"] < date('Y-m-d'))
		{
			echo '<body onload="alert(\'La date de fin de la liste doit être supérieur à la date actuelle.\')">';
			echo '<meta http-equiv="refresh" content="0; URL=modifListe?idListe='.$tokenModifListe.'">';
		}
		else
		{
			// enleverBalises($_POST["titreListe"]);
			// enleverBalises($_POST["descListe"]);
			// enleverBalises($_POST["dateExpir"]);
			
			$listeId = Liste::where( 'tokenModif', '=', $tokenModifListe)
				->first();
				
			$listeId->titre = filter_var($_POST["titreListe"], FILTER_SANITIZE_STRING);
			$listeId->description = filter_var($_POST["descListe"], FILTER_SANITIZE_STRING);
			$listeId->expiration = filter_var($_POST["dateExpir"], FILTER_SANITIZE_STRING);

			
			$listeId->save();
		
			echo '<meta http-equiv="refresh" content="0; URL=modifListe?idListe='.$tokenModifListe.'">';
			echo '<body onload="alert(\'Votre liste est maintenant à jour. Vous pouvez ajouter des items.\')">';
			
			
		}
	}
	
}
?>