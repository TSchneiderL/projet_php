<?php
namespace projet_php\controler;

use projet_php\modele\Item;
use projet_php\modele\Liste;
use projet_php\modele\Reservation;

class reserver_item {
	
	public static function prendreItem($idListe, $idItem) {
		

		$item = Item::get();
		$liste = Liste::get();

		$getListe = Liste::select( '*')
				->where('tokenConsult', '=', $idListe)
				->first();
		
		$getItem = Item::select('*')
				->where('id', '=', $idItem)
				->first();
		
		$newResa = new Reservation();
		$newResa->nom_user = filter_var($_POST["nomResa"], FILTER_SANITIZE_STRING);
		$newResa->message = filter_var($_POST["msgResa"], FILTER_SANITIZE_STRING);
		$newResa->id_item = $idItem;
		//Passe l'item a reservé
		$getItem->estreserve = 1;
		
		$newResa->save();
		$getItem->save();
		
		echo '<body onload="alert(\'Cet item est reservé ! \')">';
		echo '<meta http-equiv="refresh" content="0; URL=vueListe?idListe='.$idListe.'">';
		
	}
	
}
?>
