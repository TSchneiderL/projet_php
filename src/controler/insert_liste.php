<?php
namespace projet_php\controler;

use projet_php\modele\Utilisateur;
use projet_php\modele\Liste;

class insert_liste {

	
	public static function addNewListe() {

		$utilisateurs = Utilisateur::get();
		$listes = Liste::get();

		if ($_POST["dateExpir"] < date('Y-m-d'))
		{
			echo '<body onload="alert(\'La date de fin de la liste doit être supérieur à la date actuelle.\')">';
			echo '<meta http-equiv="refresh" content="0; URL=new_liste">';
		}
		else
		{
			
			//Generation automatiqu de token
			$generetokenConsult = uniqid();
			$generetokenModif = uniqid();
			
			$newListe = new Liste();
			$newListe->titre = filter_var($_POST["titreListe"], FILTER_SANITIZE_STRING);
			$newListe->description = filter_var($_POST["descListe"], FILTER_SANITIZE_STRING);
			$newListe->expiration = filter_var($_POST["dateExpir"], FILTER_SANITIZE_STRING);
			$newListe->tokenConsult = $generetokenConsult;
			$newListe->tokenModif = $generetokenModif;
			if(isset($_SESSION[ 'idSess' ]))
			{
				$newListe->user_id = $_SESSION[ 'idSess' ];
			}
			else
			{
				$newListe->user_id = 0;
			}
			
			$newListe->save();
		
				
			echo '<meta http-equiv="refresh" content="0; URL=modifListe?idListe='.$generetokenModif.'">';
			
			
			
		}
	}
	
}
?>
