<?php
namespace projet_php\controler;

use projet_php\modele\Utilisateur;

class cnx_user {

	public static function cnxUsers() {
		

		$utilisateurs = Utilisateur::get();

		$cmptPseudo = 0;

		//récupere le pseudo de l'utilisateur
		$verifPseudo = Utilisateur::select( 'pseudo_utilisateur')
				->where('pseudo_utilisateur', '=', $_POST["pseudo"])
				->get();

		$verifMdp = Utilisateur::select( 'mdp_utilisateur')
				->where('pseudo_utilisateur', '=', $_POST["pseudo"])
				->first();


		foreach($verifPseudo as $nbPseudo)
		{
			$cmptPseudo++;
		}
			
		if($cmptPseudo == 0)
		{
			echo '<body onload="alert(\'Pseudo utilisateur innexistant.\')">';
			echo '<meta http-equiv="refresh" content="0; URL=login">';

		}
		else if($verifMdp->mdp_utilisateur != $_POST["mdp"])
		{
			echo '<body onload="alert(\'Mot de passe incorrect.\')">';
			echo '<meta http-equiv="refresh" content="0; URL=login">';
		}
		else
		{
			
			$getId = Utilisateur::select( 'id_utilisateur')
				->where('pseudo_utilisateur', '=', $_POST["pseudo"])
				->first();

			$_SESSION['idSess'] = $getId->id_utilisateur;	
			
			echo '<meta http-equiv="refresh" content="0; URL=index">';
			
		}
	}
}
?>
