<?php
namespace projet_php\controler;

use projet_php\modele\Item;


class update_item {
	
	public static function majItem($idItem) {
		

		$item = Item::get();
				
		$itemId = Item::where( 'id', '=', $idItem)
				->first();
		
			//Enleve les balises html entrés dans les champs
		$itemId->nom = filter_var($_POST["nomItem"], FILTER_SANITIZE_STRING);
		$itemId->descr = filter_var($_POST["descItem"], FILTER_SANITIZE_STRING);
		$itemId->url = filter_var($_POST["urlItem"], FILTER_SANITIZE_STRING);
		$itemId->tarif = filter_var($_POST["tarifItem"], FILTER_SANITIZE_STRING);

		$itemId->estreserve = 0;
		
		$itemId->save();
		
		echo '<body onload="alert(\'Votre item a bien été mis à jour ! \')">';
		echo '<meta http-equiv="refresh" content="0; URL=mesListes">';
		
	}
	
}
?>
