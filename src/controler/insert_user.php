<?php
namespace projet_php\controler;

use projet_php\modele\Utilisateur;

class insert_user {

	public static function add_user() {

		$utilisateurs = Utilisateur::get();


		//Vérification qie les mots de passes soient les mêmes
		if($_POST["mdp"] != $_POST["mdp2"])
		{
			echo '<body onload="alert(\'La saisie de votre mot de passe doit être identique, veuillez recommencer.\')">';
			echo '<meta http-equiv="refresh" content="0; URL=register">';
		}
		else
		{
			$cmptPseudo = 0;
			
			//Préparation de l'insertion d'un utilisateur dans la base 
			$newUser = new Utilisateur();
			$newUser->pseudo_utilisateur = $_POST["pseudo"];
			$newUser->mdp_utilisateur = $_POST["mdp"];
			$newUser->statut_utilisateur = 2;

			//Requête qui vérifiera si un pseudo est déjà existant
			$verifPseudo = Utilisateur::select( 'pseudo_utilisateur')
				->where('pseudo_utilisateur', '=', $_POST["pseudo"])
				->get();
			
			foreach($verifPseudo as $nbPseudo)
			{
				$cmptPseudo++;
			}
			
			//Si aucun utilisateur ne possède déjà ce pseudo alors on le crée et redirige à l'espace de connexion
			if($cmptPseudo == 0)
			{
				$newUser->save();
				echo '<body onload="alert(\'Votre compte a bien été créé. Veuillez vous connecter.\')">';
				echo '<meta http-equiv="refresh" content="0; URL=login">';
			}
			else 
			{
				echo '<body onload="alert(\'Nous sommes désolé, ce pseudo est déja pris. Veuillez en choisir un autre.\')">';
				echo '<meta http-equiv="refresh" content="0; URL=register">';
			}
				
			
			
		}
	}
}
?>
