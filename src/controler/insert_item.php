<?php
namespace projet_php\controler;

use projet_php\modele\Item;
use projet_php\modele\Liste;

class insert_item {
	
	public static function addNewItem($tokenModifListe) {
		

		$item = Item::get();
		$liste = Liste::get();

		$getListe = Liste::select( '*')
				->where('tokenModif', '=', $tokenModifListe)
				->first();
		
		$newItem = new Item();
		$newItem->nom = filter_var($_POST["nomItem"], FILTER_SANITIZE_STRING);
		$newItem->descr = filter_var($_POST["descItem"], FILTER_SANITIZE_STRING);
		$newItem->url = filter_var($_POST["urlItem"], FILTER_SANITIZE_STRING);
		$newItem->tarif = filter_var($_POST["tarifItem"], FILTER_SANITIZE_STRING);
		$newItem->liste_id = $getListe->no;
		$newItem->estreserve = 0;
		
		$newItem->save();
		
		echo '<body onload="alert(\'Votre item a bien été ajouté ! \')">';
		echo '<meta http-equiv="refresh" content="0; URL=modifListe?idListe='.$tokenModifListe.'">';
		
	}
	
}
?>
