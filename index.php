<?php
session_start();

require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use projet_php\outils\Outils;
use projet_php\modele\Utilisateur;
use projet_php\vues\login as lg;
use projet_php\vues\accueilInvite as accInvit;
use projet_php\vues\accueilUser as accUser;
use projet_php\vues\formCreaCompte as creaCompte;
use projet_php\controler\insert_user as newUser;
use projet_php\controler\cnx_user as cnx;
use projet_php\controler\deconnexion as deco;
use projet_php\vues\creerListe as nouvListe;
use projet_php\controler\insert_liste as addLis;
use projet_php\vues\modifListe as majListe;
use projet_php\controler\modifier_liste as modListe;
use projet_php\controler\insert_item as newItem;
use projet_php\vues\mesListes as listesPerso;
use projet_php\vues\consultListe as maListe;
use projet_php\vues\modifItem as monItem;
use projet_php\vues\supprimerItem as supItem;
use projet_php\controler\update_item as itemMaj;
use projet_php\vues\consultListePublique as listPublique;
use projet_php\vues\partagerListe as sendURL;
use projet_php\vues\reserverItem as resItem;
use projet_php\vues\supprimerListe as supListe;
use projet_php\controler\reserver_item as resUnItem;



$app = new \Slim\Slim();

$db = new DB();
$config = parse_ini_file('src\conf\conf.ini');

$db->addConnection([
'driver'    => $config['driver'],
'host'      => $config['host'],
'database'  => $config['database'],
'username'  => $config['username'],
'password'  => $config['password'],
'charset'   => $config['charset'],
'collation' => $config['collation'],
'prefix'    => '']);

$db->setAsGlobal();
$db->bootEloquent();


$app->get('/index', function () {
if (!isset( $_SESSION[ 'idSess' ])) 
{
	Outils::headerHTML("Accueil", "index.css");
	accInvit::accueil();
	Outils::footerHTML();
}
else
{
	
	Outils::headerHTML("Accueil", "index.css");
	accUser::accueil();
	Outils::footerHTML();
}
});

$app->get('/login', function () {
if (!isset( $_SESSION[ 'idSess' ])) 
{
	Outils::headerHTML("Login", "login.css");
	lg::logUser();
	Outils::footerHTML();
}
else
{

	echo '<meta http-equiv="refresh" content="0; URL=index">';

}
});

$app->get('/register', function () {
if (!isset( $_SESSION[ 'idSess' ] )) 
{
	Outils::headerHTML("Register", "inscription.css");
	creaCompte::creaCompte();
	Outils::footerHTML();
}
else
{
	echo '<meta http-equiv="refresh" content="0; URL=index">';
}
});

$app->post('/cnxUser', function () {
if (!isset( $_SESSION[ 'idSess' ] )) 
{
	cnx::cnxUsers();
}
else
{
	echo '<meta http-equiv="refresh" content="0; URL=index">';
}
});

$app->get('/logout', function () {
if (isset( $_SESSION[ 'idSess' ] )) 
{
	deco::deco();
}
else
{
	echo '<meta http-equiv="refresh" content="0; URL=index">';
}
});

$app->post('/addUser', function () {
if (!isset( $_SESSION[ 'idSess' ] )) 
{
	newUser::add_user();
}
else
{
	echo '<meta http-equiv="refresh" content="0; URL=index">';
}
});

$app->get('/new_liste', function () {
	
	Outils::headerHTML("Nouvelle Liste", "creerListe.css");
	nouvListe::addListe();
	Outils::footerHTML();

});

$app->post('/newListe', function () {
	
	addLis::addNewListe();

});

$app->get('/modifListe', function () {
	//accès seulement aux utilisateurs possedant le token de modification
	//soit les créateurs car le token de modification n'est jamais partagé
	$tokenModifListe = $_GET['idListe'];
	Outils::headerHTML("Modification Liste", "creerListe.css");
	majListe::modifierListe($tokenModifListe);
	Outils::footerHTML();

});

$app->post('/modListe', function () {
	$tokenModifListe = $_GET['idListe'];
	modListe::majListe($tokenModifListe);
});

$app->post('/addItem', function () {
	$tokenConsultListe = $_GET['idListe'];
	newItem::addNewItem($tokenConsultListe);
});

$app->get('/mesListes', function () {
if (isset( $_SESSION[ 'idSess' ] )) 
{
	Outils::headerHTML("Mes Listes personelles", "listes.css");
	listesPerso::voirListes();
	Outils::footerHTML();
}
else
{
	echo '<meta http-equiv="refresh" content="0; URL=index">';
}
});

$app->get('/consulListe', function () {
	//Consultation des details de la liste selectionnée. Utilisation du token de modification en paramètre
	$tokenModifListe = $_GET['idListe'];
	Outils::headerHTML("Consultation de ma liste", "listes.css");
	maListe::detailListe($tokenModifListe);
	Outils::footerHTML();

});

$app->get('/modifItem', function () {
	//Modification de l'item selectionné. Récupération de son id et
	//du token de la liste
	$tokenModifListe = $_GET['idListe'];
	$idItem = $_GET['idItem'];
	Outils::headerHTML("Consultation de ma liste", "creerListe.css");
	monItem::modifierItem($tokenModifListe, $idItem);
	Outils::footerHTML();

});

$app->get('/deleteItem', function () {
	
	$itemId = $_GET['idItem'];
	supItem::delItem($itemId);

});

$app->get('/deleteList', function () {
	
	$listeId = $_GET['idListe'];
	supListe::delListe($listeId);

});

$app->post('/changeItem', function () {
	//Mise a jour d'un item d'après son id, mais n'etant accessible
	//seulement en etant sur la page modifItem auparavant
	$idItem = $_GET['idItem'];
	itemMaj::majItem($idItem);
});

$app->get('/vueListe', function () {
	//Visualisation de la liste rendue publique, donc son token de consultation
	//a été partagé par son créateur. Peut être connu de tous.
	$tokenConsultListe = $_GET['idListe'];
	Outils::headerHTML("Consultation de la liste", "listes.css");
	listPublique::detailListePub($tokenConsultListe);
	Outils::footerHTML();
});

$app->get('/partageListe', function () {
	$idListe = $_GET['idListe'];
	sendURL::urlPartage($idListe);
});

$app->get('/choisirItem', function () {
	//Reservation d'un item. Obligation de connaitre l'id de l'item
	//ainsi que le token de consultation de la liste
	$idListe = $_GET['idListe'];
	$idItem = $_GET['idItem'];
	Outils::headerHTML("Consultation de la liste", "creerListe.css");
	resItem::prendreItem($idItem, $idListe);
	Outils::footerHTML();
});

$app->post('/faireResa', function () {
	$idListe = $_GET['idListe'];
	$idItem = $_GET['idItem'];
	resUnItem::prendreItem($idListe, $idItem);
});


$app->run();	
?>
