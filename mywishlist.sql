-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 19 mai 2019 à 17:53
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mywishlist`
--

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text,
  `img` text,
  `url` text,
  `tarif` decimal(5,2) DEFAULT NULL,
  `estreserve` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `img`, `url`, `tarif`, `estreserve`) VALUES
(1, 2, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 'champagne.jpg', '', '20.00', 0),
(79, 24, 'Boisson', ' Coca, bière, alcool...', NULL, 'http://pizzalenomade.com/cessy/wp-content/uploads/sites/15/2015/07/boissons-bouteilles-1-5L.jpg', '50.00', 0),
(80, 24, 'Gâteau apéritif', ' Toutes sorte de gâteau apéritifs. ', NULL, 'https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Ffac.2F2018.2F08.2F01.2Fd49be3be-5041-417c-a8ae-079adfd0111c.2Ejpeg/748x372/quality/90/crop-from/center/les-10-gateaux-aperitifs-les-moins-caloriques.jpeg', '20.00', 0),
(81, 24, 'Jeux de carte', ' Un jeu de carte de 32 cartes.', NULL, 'https://cdn.toutpourlejeu.com/4715-large_default/jeu-54-cartes-francaises-classiques.jpg', '5.00', 0),
(82, 24, 'Mallette de poker', ' Mallette de poker simple.', NULL, 'https://joueclub-joueclub-fr-storage.omn.proximis.com/Imagestorage/imagesSynchro/0/0/4010498e3e9410a69cd880f10326417136089b69_69090.jpeg', '30.00', 0),
(83, 24, 'Boule a facette', ' Une boule a facette a accroché au plafond.', NULL, 'https://images.vice.com/noisey/content-images/article/boule-a-facette-2015/meet-me-under-the-disco-ball-a-history-of-nightlifes-most-enduring-symbol-1433388224.jpg?crop=0.7972508591065293xw%3A1xh%3Bcenter%2Ccenter&resize=650%3A*&output-quality=55', '10.00', 0),
(84, 25, 'Dragées ', ' Dragées blanches.', NULL, 'https://images-na.ssl-images-amazon.com/images/I/814RtctudjL._SX700_.jpg', '50.00', 0),
(85, 25, 'Medaillon', 'Médaillon en or.', NULL, 'https://www.amglacouronne.com/259-large_default/medaille-notre-dame-de-tendresse.jpg', '300.00', 0),
(86, 25, 'Piece montée', 'Pièce montée.', NULL, 'https://files.meilleurduchef.com/mdc/photo/recette/piece-montee-choux/piece-montee-choux-640.jpg', '100.00', 0),
(87, 26, 'Assiettes', ' Assiettes pour le dinée.', NULL, 'https://images-na.ssl-images-amazon.com/images/I/71fQ6d3MRXL._SY355_.jpg', '100.00', 0),
(88, 26, 'Buche de noel', ' Buche de noël', NULL, 'https://4.bp.blogspot.com/-jog3CvMDcJI/WFsJKp3z2vI/AAAAAAAAVoU/J8Fx4rZ7rZkzoAfgnQf6XAp8G-eW0h6qgCLcB/s1600/buche-facile.jpg', '30.00', 0),
(89, 26, 'Foie gras.', ' Foie gras.', NULL, 'https://www.foiegras-groliere.com/423-large_default/4-blocs-de-foie-gras-de-canard-130-g.jpg', '50.00', 0),
(90, 26, 'Dinde de noel', ' dinde de noël', NULL, 'https://www.lidl-recettes.fr/var/lidl-recipes/storage/images/france/recettes/dinde-de-noel-aux-noix-de-pecan-et-canneberges/1147076-1-fre-FR/Dinde-de-Noel-aux-noix-de-pecan-et-canneberges.jpg', '50.00', 0),
(91, 26, 'Escargots', ' escargots', NULL, 'https://www.ambiance-noel.fr/medias/article/31/image/comment-choisir-les-escargots-pour-le-repas-de-noel_500.jpg', '20.00', 0),
(92, 26, 'Huitres', 'Huitres', NULL, 'http://fishandfiches.com/wp-content/uploads/2014/02/01-940x627.jpg', '20.00', 0),
(93, 27, 'Bougie ', 'bougie enterrement', NULL, 'https://torange.biz/photofx/121/8/dark-blur-frame-funeral-candle-121471.jpg', '30.00', 0),
(94, 27, 'Fleurs', ' enterrement fleur', NULL, 'https://www.agitateur-floral.com/823-large_default/coeur-de-deuil-blanc-douceur.jpg', '100.00', 0),
(95, 27, 'Cercueil', ' Cercueil', NULL, 'https://www.simplifia.fr/info/wp-content/uploads/2015/04/cercueil.png', '100.00', 0),
(96, 27, 'Pierre tombale', ' Pierre tombale', NULL, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMstxWJ3UI6lbMkLo_G1HGPQ1-yeikdZfwhWvkkHYFiA9KpO0p8g', '900.00', 0),
(97, 27, 'plaque enterrement', ' plaque enterrement', NULL, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExMWFhUXGBcZGRgXGBgYGhobGxsaGBoYGBkYHiggGx0lHhcWITEhJSkrLi4uGB8zODMtNyotLisBCgoKDg0OGhAQGy0lHx8rLS0tLS0tLS0tLSstLS0tLS0tKy0tLS0tLS0tLS0tKy0tLS0rLS0rLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAAAwQFBwECBgj/xABGEAABAgMFBQUECAQFAgcAAAABAhEAAyEEEjFBUQUiYXGBBhORofAyQrHBBxQjUmJy0eGCkrLxFSQzQ6IWU0RUY3OjwtL/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EACQRAQEAAgICAgICAwAAAAAAAAABAhESIQMxQVETImGhBHGB/9oADAMBAAIRAxEAPwC8YIIIAggggCCCCAIIIIAggggCCCCAIIIIAggggCCCCAIIIIAggggCCCCAIIIIAggggCCCCAxA8azZiUglRAAxJLARxm3PpEssklMsGcofdonxz6CA7R4wpYGJAiltq9vrbN9lfdJOAQGPBya+BjlrVtObMLrmLVqSo+NThWNaR6MNvlD/AHEfzJ/WFET0HBSTyIMeY5k4uxLtTE/u0aSrVMSQQo0rQkF9MYcR6ieB4872DtZbJIT3domYsyjeA0cHKOt2H9LCwWtMsKA9+XQkas7fCJpVuPGHiL2Jt2z2tF+RMChmPeHMRIxBu8DxGbX23Z7Mm9PmpRoCd48gKmOA2z9LqEm7ZpJWfvTKDmEip8RFk2LSeMvFBbS+kjaC3aaJb4BCQPMuYhD2qtqgb1pnFWPtmGkelr4jN4R5cm7Rnkn7aaSpwSVqphxhSTtCbRImzAc2Wp+GcND0+8Dx5osnaG1pIa0TeG8r58olbN292gg0nqV+YXvjDQ9BPA8UxZfpYtSf9SVLUOoPHOJ2xfS1JP8AqyFo4pIUD0xhqqsp4HjkbB9Imz5rNNKSclpIiYs/aKyL9m0Sj/GAfOIJZ4y8N5c9CvZUk8iDCgMAo8YeNYRm2lCaqWkc1AQDh4y8Qtq7R2ZH+4FHRNceOEIbJ7RifNuJQwY1Jq45RdUdC8EadIIgqj6VNozhaBLvHu2onJ7oJfXERxVjKVqN4DoSG0xjrvpWlmZPugOx8ihEcd9XKc3JemHPOuPzjc9ISt4u00fIV8ISBo9BUPwDZa1EOFEBQGJzBLDm9SR6pGlvQTd3CHTus5oCXPiD5wCMzU8DkeDemhtNS5F3HHHjx+EbGaQ+JegIwFHMJKLji7csH54wUEtmSAR/eNCa0HrWMlak0LMM6t4xI9nrGJswqmbsqWkzJhyuJyGhJoOfCJllMZukm+iezdpzbLMEyStSVAvTA0wORjs9qfSnapiAiUhMotvLG8TxSCGT5xwNonArNwXQ6rqScAXYc2+EIzDh8a4GEDi22mZMUpUyYpanqVEqNflDSVNdWIHwr6ML2GxrmqSiXVSi1cNTXQDOH21tmy5KJQQq+VhSnahF5kkZ1Yl8wRE5SXS6vtHziksBrkfnGgAD6V6+esZmFJIYMWY51fHXMRpOoRgaVIf07mLtGxU1Sbpz9Z4RhRqKPQ019FoTSsZmozIPX5xuXc71XxGurnD+8EYUVBQJVzcQouZUEmpy+UNu5JzoTl+8KhGtCDhjT9IoVKajIH1XwjYywVUfp69NCN8u/kHPhlr4wJOJfw9GJtTu4xLgdPNq+mhRIrlX0whqqaz1x6VHCMpX6xgJCVaCKAkHm3E4eEPv8Wms5mLHALUPgeXiYigBdZg+uYAGHWN0rcjkA3jXyiielbTm+yVqJ/MTjWvp4zO2gpwXJAzfMU864axGS2ZTEMMcs8fhGs2cSQAaAVflFErOt5apqqpIpQZE/KOr+jifetLv94DldJ/SK9FoOJxDUxeuhpHcfRe/fgmu8aigqknDrBFuQQes4I5qqz6QpSTNmKIchaW/lD8cniv5qndqVLvWnPLOLA+kJQKpnCYH8D+0VtMmkKqxIOtDj+8b+EZChdJxLUIzq2HjSJhr9jSogkylnibi6DHHeGEQS1YkOPWMTNntik2e4Eg94koUXZjed65MDo1Y5+S9b+m8ULOSAogFw7EjFtH0/SEilw45+nbAw/s9rsgKgvvZiEjeXKCakkJaXea8zvgzDjDW0KkoQlcs95efu0rTdLJJBVMB90cMTQGhi8k4k5qUgOtRZTsGdRr7reDmkSM+3oTYxIli6ueQqa5DhCCQlOlTVoirT9igTpoMydNG4lWgLd4oUZOSRhjpC1i7U7QlsRMlsCwSqVJIbhuuRQ4eMc7+zU6Id0Kn3sPXxjXhVvWvI+MS83bEi0j7WUiROZwuU/cq/Ok1lniHEOez/ZyZaDeYplocqXyGWRPyjdzknace2dmWa40l2VMRfnqFO7kY3eClthpzrDbR2kZ8xUzBKiAgaJAupHgIe7fn42Ozglay8266lcJdHJOZ8Ijrfs9chQRMa8wJCSkkaA3XY8I54d5cr/xrPqaN0e1RwWw46Y5RlNcv2MJGd8KlmgKgaCoyZ+dY7OZeWcc/2bDhhD2wWBK5U+cq99l3YARddSlqIaoIwClP+Hi8RRms2RfLj8Ik5W1wizJkyypKjMK1kEpBJASgAgvui9j96M5710sLW7Y6pU5UtBKykIchLEKWAq4oB2XW62o6Q3VsycAVGWpkhZUWwu7qnI0JAPOHmydrSJYkXkrPczTNWBdurO6UKclyUlIDdXqRCczbAWqQhQV3UveWKOtSllcxT5ghTAHKOfLP1prpjaWyyiaZckKUU3Ap2O+pIUpIbFiSkDNjDVVgmhLlBu3QbzUqWB0YkNXjDy07eQU0vJmibNmXgEm93jMbx9hQqHAOOMN7dtEzDLSgKCJcqWhIqzgbxLZlTmvCGNz6lhdFtnWFU2ahCQWKkoUaUcuouqlBXlCc+UouoJPdpoFM2691Ki3JudIkJO2JSEpISu8mzqlAbrJmKCr6wXrevPw40hvtbaqVuZZKUqCAUMkAXUgBDgkqAyw5PFmWdy9dGpoiiwzO7E0g3L1wEYksSwA+LaawvKs29dKVJDFRoVboBdwBg4Z+fGFbLtiWhMpICryEz3Iai5gYLFaskJGVQWyjP+LoulCQRdkKQgsKqWp1qOgIJDCHPP6NQ2mzOoocGjQKcaZD9IbX1GtKeupjKJzPVxnU58tKCOzBZbjAvw+FdI776KJjzEuf9xTfyRXhOBJA5euUWL9F8x1y2FO8V/S2PSFSrfeMxo4gjJ2qf6QQ/wBYIp9oD4KAitVkgknrnxizu3dU2oHC8fC88ViACaNhrXn5xoOpdnKpMycCAApCaYm9eNH/ACwqtAKRLwBSHAoWIClnnvSxyBhzsVANknpNTfkKAIf3lJIb+KErHY0zbbNXNN2VIK7x0AJdtT7IAzpHHLLu7+HWQ0s+ylKUSB9kgFAUMCstfbXeDckCJmx7GTN2jNSvCQEhWiUJSA2lXduJMMJm3VW22SpSPs7MiZLuy0kgEX0uVqHtKzbCueMTvam2JkptzU75cuXezKVoS/gkzD/CI43LLevtuSfHw4naO05k+cu0IuJTeuoChfZAcJZJ3QAAMc1Uzh5aZ85UyXLMsTlqQkqAF1V5TkhIAYUo3CHM7sxNRaBIO8BdcJzCgF3cMS7QptLavcKmSZKhfdrRPHvKLvLQckj2WzY5AkdbZjJjGZL7p3ZOyCQoKtVol2dNCUKVem8UtLdueUO+1G25kqUlNmCUWcC5LVLU6EjAqmGhK9A1KljSOGtU6YpW4SlP37zPqbyjXpT4w/7P25SLwW0yWR9qkEKCk/fDYKTjxDtw55Y5XvL4bmp1GyNppkpKLM4Kn7yef9RZONx/ZTxxMR6NS5JOLueuZib2zstMgpUlV+XMF5BGmh4injEWlPrxjvhMfcccrfVN1Eka+sPKN0hkhwcdPh0hay2ZUyYmWiilqCQ+FSw5Coha3S5Sf9MrKgsgu11SQBVsRXKtGrFuUl0mjFAILl68iXNI0mqemI9YfCJ1dkCvtp6yJk9K1gJZ6OEbrVdaeDAYvDiRsaV3lnCipaJiVTFKBAF1CCZiWZ0lKgUtyL1pj80jXGuclSmUAf1+HMQXiki8K8RSriJfZCULmTCtDy0S5s0oBZ29lF5qgqUlLtnBatly5SSJit9kECh3lMq6oAOAAcXxoBF/JN6Tj8oYSd58dGxLQrKWalhX9z8sY6HZlhSJJuumYqYr7S8GTLkJ72YpIA1KcDUgCmZY9loHdTQVh0zppe6q6iWN1V1SSDeVRiG5xL5YvFAKlsHoz8c39NGEkNQHPr6pE0NkI3UlZEzuROmDdZIa9XCpSUkfnqYadwFrlS5RKlLuh3BAUokMCwcYVGpxYGLPJjU400lpq7GmeHkc4T7suAanEYMQ8TMrZ0lU2XJK1OtakEhi2F1bMwDvuuSzHhGZVlQETlXlC73cveSlTlRJUwI3WCCXBfLOH5YcahTNUR8QMPWMZS+Z9cI6KZsuWhc0rKlJlySsEEMq8QmSUkCoVfB4MdIQOxUd4mStREwzJaCEkEVqsnQI1OLGghPNicahkppvZk4/Hz8os76NksZLYd6rPmIre2d2lu7UpVKkhszhWoZq8cosX6N3u2d6faGnC8Y6y77jFW68EZaCIbVd2ydQtQb/ALpeuT+eWMVd3Z/TpFqdow6rSNe9Hkf1ipJ4IPwD5GAlezyliYEpNFMVEh3CDfYcaGNdu2gPLkAsZ6lTV44G9dHLE9RDnY00KlgJSL8tYY5lE0FBB5KYucL0Qe25hNrWo0ulKB+VIuU6hUcLJc3abkKbIRctaXJZMxDasSln6V6R03aBSl2gygi+QpRBV7KQlVwLOZISKAcY5sTt4TAHONNaJHhHZ2rYNonpmLs6SVzmS/s3UlrxLsymUqmI6xz8t1lLXTGdXSLk7S7uUuff31XpUtZLhKUsmZMH3lYIBzJGQDcWucFFwmjm7Ly4qmEYuWB1wDNTrO0PZe2JBQmUClAShN1SCAlOdFZlz1Mc9ZdmKSsJXSoD1NSwcAVLCrcNY34rj3dsZ79G0yzhw+8QBxajskZAYUwaFJKbq0rQGUkgjAPwLZHA6iJnaUv6qjuBW0zGM1X/AGkGolDK8R7RyFIjpSkA7zsDUZ4UxPSOsy5Rzs1U/tCanuFyvaEmYDL1CJiXudCSOQjnbyi+mJLevRhfv75Wo++b1OoFNA/lDeaqnli1MInjxuM7M7u7bfWrqrwBcMxdiDkoEVBpTSN9oW9UxW+oqLVPOpPU1hrKOQYODT1gIyhVA9B5xvUZOZdunABImKKB7KQogAu9Pi2DxkbQmhV4zFAsoe01Fe0KfeNSRUmuNYbLQaM7YNTH0IX2fY+9mIl3kpvKY3sBxLZUroAYzZj7XtrItkxIUA6X3TdoCHe6Rz+EKLtsyYwmLUtCbvvVYUYEvUCgfhG1o2aqWZt4hKpau7KSCXU6geAa678tYYpDGo9Z8+cJxq9n/wDi60rQUqUkSwoJYh95yq9kXcg5M2kH+KTwpr61bhRjikqvXXfB684bWaQVrCMN4BzQcHLUDRlKCMiwLNU/2MNYnZaVtKaJipjkKIZyaswQx1DUbSkHeqdMwKN6hcEghi4rkaFoQcgOw40PXrw5Q62dYzNmIQDdvEJdiw4lov6ybRhW0ZpJUVEuQTlhWjM1cWxzjKtpznJK1gqVfxoVMQ9cmekJzpDNdUCCHpkHauGjxrMSaAMp2cth+GJONXtv9fnb4vqAUkJUAwBSk7qeAGg5Qqrak1QvFW8xD0vYXSS1SWcOXMMcCa1+cbEHF8Oka4z6TdZRMAoW5tXVm8otP6PVFrLoVH+pUVZmH9eni1OwrNZKVBTxxUc+vxjbK2WGkYjN6MRlVbbVczZ1D7S3wrlFPrZVXw4VPDgf0i4Nop+2m1Z1LbxP7xUimF5LvWo5FhXXGCHnZ5RvTAC24TozKSp/+PnCnaezG+FJB374pmy+8/8AvGuyBdRPVgRLUBo6yEJ/5KETVstQ+qon0CpctN3N5kwJTn927e/hjzeTPjn074T9SOzptjsKQbSRMnDe7pDEp/OTROPOuEOR9JFrmOJEmVJSAQCoKWql0DMAe2MjFarBIBLkzF1JxIS2Jzcq8o72z2JKJExRS9MmckrUkAcWA/ljn5fHj1ll3W8cr6iA2l2m2hMW67VNSlT0RuBuAS0L2YmUjvjMVeSQUlRvEzDvJG9okAnS9zjWdY3mpF2gDkclFATq5UAnrD/YwQuaufNN+RZElZbCbMfL866DHdSI63jJ0xN77M9q7PVKShUxRM+bemLQ7qCCxSpZI9pW8W0iOWzbwHn8/wBIVtdsmzpkyYv21m8rmTRI4AMOkJJQSQWccTl6B0jthLJ253W+hKUT0GDVppkITW/j+uucbzXJdONefLnCS3IZsOPLKNIVApgxwHDm3SNQqmZJYB/XCMKUQS7DLl0MJImkZvjWAUS1D0bH1/eH1jIAWq8Aq4bocubxAVlQ3SuurQ0kpBUCGArU1w9CEVAu2T/LPyjNmyOrtG0kTJkpawyiglaiCUd+Ed3LUqhpupVmxUqIe2zu9UlJUlkpCXSFMlySVC9vHElzU8YjyFAY54c6RsC1SQ7fGkYx8UjVydIdvhU+0TCopl3ZncpINFFPdJ/iuklzoBlGJVvkd5LQhYElSZIUAFXw11S7z7oN4EkhydWw5pVWatR4E4MOkaJoQQcMvjnE/Dicq6Hbe1hOlgAspc6bNWirIeiAMB98lsSp4cybfJloBRMqLLdSkA1mqSEzL1Gvby2yYJrQRzhWK0Z9fh0OkalZbAGrH0Djn1hfDNaOXe3RWraiECYJa7x7uRLQWO6EVUqre8kcd4whtS3pIIlqF1aUA0VfyWu9gkErq4clxVohCCGf1rGO7Jr09F+PlFnixhcrWQK4VPH5PhhGwGJ9MY0lsRj5M3XpGxBBY0phnyjqwWq7atQZZvFpdji31UYVlU6iKnUTwf1jWLa7L+3ZQasZWPSvnBFsesIIzBE0Kxt9J0z86v6i/l8Iqe2Fps1Idr6xpgrllWLYtc37SYX99WWTmKr24Ls+d/7q26qP6+cUSEuXd2bPmYEzJQb8KFyyfNUP51n7yxoSHJTPUTTECWu7/UPCGmx1ibIVZ/v94kc1pSqX/wDJKPiInOyqvs5N9NFJS/NJ7pRPx6x4vLe7v4r1eOdOGtFmAmyUn8R098AU/gMdX2gS1nTLCgCucwL+6gEuP4iRDDaNmAtqEtgSC+QQuYok873kYzty0nvrKCBdlS1TVgu133natQVdSImX7XGHrdZtqglIymTFFAOYvEqNMrqFnqtB4RCpsV2UDMKgk3WSCwUoVdQNAlDnJ36QqFqmTyThLCg2V4grUByKQgcECENsT78whxdR9mlsGFCoDiQTybSPRjPhzyvyaTSQWBxZ4UJq9WpzA5PrGQBdyJL4jB84SmXianKjaYR2c2wUasdXwbSj54wXwCXNGqejZHX4Rlctqk+VBWkaFOYetWz5P0IgE5cvLM9enwjZD4ZA4HPh5RmVQvr8dDGFEhw2LY8s+sQPtlbNXOKw4SEJKzR90KSlRAeoF5zoATVoWTsg3b61BKSlSk3qBSUkgFyzXiCEgOToBWG2zdrLlABASVAkhRd0ki4auxDHAgjnCk/axmIlpKUbiQkKKS5SkkgGrUwcB8Kxyv5LemprR5aOz5lruCZLKgLywLwKEFAWpSqUZwGqSSNRBL2YE/aOlQEhU5lBXsv3aAoJ94qa7VqDKkMpe1pneTpjJWqcFXwpJIYqCzdY0YpTRzhV4LVtybNStJCB3kuXLJCWZEv2WqwNATRjiGpE15DcZ2xYkyVpQlSlKMtClukBisXgkB67qh5wta7PLkWgywnvlJTdUFhh3hSymH4SXHFMM7VtFU2cZ5AC76VUDJcMwZ3ILAM+BhCbaVLnd6aKKipTUALu/iM9I1Jlrv6/s3EsvYq7ywSm9LVLChW6DMN0AEVKgcRrQXqstI2IoT0hSkqT9YXLJahEvemTAD7oHqkMZO3ZgX3iQn/UE1QYspbuCpy7VLAFg5aFBt2aQk7gCRMSAzUmuF1d8CzhjQOdc68q7xScuwJM0Tz3a5Skz5rNRkXkhBSoYXrgGoORhsdkquIYOO7TMUUglRCybqSD72DMwapiPm7TXcVLTduKQEXWG6kKv7n8VS7nON5naCYbwIQoKShJTdVdAli6lSWIILPmXq8ZmHkhyxO52xUpSZhnSwgLWgGpCihIVu3QXqQNHzbGLKEktjWreucZtFtVMloQWCEFZAFAStV4nR3YcgBDcKoWoA/jxjthLPbFs+G4UCeOpNOL+UWrsMtMs4pRUrDmnXHOKiUHemR9Ui5NmpCZ8kNTvJQ194Zx0ZWw/p4IOpgiCrbSftVXnAvqd+Cj+0Vp2jT/AJmawYFT4tiAfjFmWxX2iz+Nf9Ry+UVx2mlf5pQAJJutzIAHHKLViMkzShiKYdCDeSfGsdhs+el5b072bNF37veSxu/wzkqHWJmV2HkizoK1pTNCElSCLxKjiCymGOUcrb7KqXMSokkS5qT/AEi8XqxATXUnmfJlcc5t3w3j1WdvzP8AMqVie7JfmgBPwmRnbYSO7CAVTZhQkpHtXEC8w5ruvwBjTbVmKkGalqm7jledm0CT5wJsSpgJCrsyYCxZymXUXRoVEVOSRHKfFdL8kJQRvIlBSj7ywN0qAwSffLBiwYXsTEYnZE9V0dzNoGpKWfgK5Ujttj7OSN1LJYk1bA3gaODqP7RJWRSUzjLE2UTiUSyrdwe8kKLE6eUby83D1GZ4uXtWs/ZNoQklcicgCl5cpaRwqQOEMrwPQVz1i2ttbGt04Lly7XKTJXTulJKVXdCu6SedIrzbnZ6fZVDvUAAksobySc2I+cdfF55n9OWeHFDKLnBwICpgxe9RvjXyjbvfaLg0DPThTjDQklJI4P0/WkdmCqrQwanU4nPh6EIJnkmvxw4BuMaKTqRh45v8IVklL9H0Apm/SKMd64HSgOPPxhRCEuGc5fsAKxP9j9sSJAmJm3EzCWStUkzmRdLpSBVKgq6rQsImZP1NZVbES3QldAAEJJSlIYj3StZoGwd2xHK+TV1Y1MdxzG0NjzZCEKXdN++GCnUghiUqHum6pJb8UMO4AU4LUf8AYvFjz5qFKuLkpUb0iWAok/bz1X1qJzYEDJ2hG1bIky1X1WdBlqKytRNEVUmVKRVgsG678mFYzPN9rwcbtDZsyUJalpAExN9NT7LtUHx6iGM0qDKbEEYGowodHiybTZEFU29JTNuKSLhfcSAUSnYgAMkqU+WjvDG0bFlMZKJSFKUl7xQQlwxUpK1UuAEABLgveJaGPlhcHMWLYs5aZS5SX79wm69ClSkF+qfKMW3YcyWqZdKVIRuqWkuBiKjRxjy1D9bs3ayFBMuzsVS0z7oBG6lN0JYZX1754PrCS5EqRKF5lJKApc0pvAk7xuTFUVoEpckkvQRJ5bvs4uIlIZIq+n9s4xNU4dgAMDw461D9YsO02ZKpm/JlJCElaShJJnLLNdAJUpCQQ7YsTR2hijtPJlKmy1qEqaQkCYJKZhwzSjdSXd01oqtY1+X6icP5cMmtHLU6+jGzg+yHGD1pn4Yxvty2yptomTJabqFKdKWAxxoMKuesNiu773T5f2jtLtilrKSVMNR50wOUXFs4/wCYlH/1Jev3hFRbOUVTZYuh76MNCoYxb1iCe9l694jxvCKi124xmNvWMYiIqO2qZcwj7yz4H0Y4PtiWtJIpuJOjY55FxHd2wG8vmvDmY4TttLV9YRoZYB6KWRjlWLVWN2U2gmbY5ajNkzJzEKvoBUk3iwKXGTVprHP9ptizZigUTEBSgQtJBS9VYO4zLV04NWaFgZKfIg4dRkYmpHa21oliSnuylIui8hV5uJCg5jx3wZy7xejHyY32k5Ww5yUpMxSQpjS+CKFhhhRvCMWOfKSDLnJPtghjQBrqSgnIAJp+8QU3tLaM+6fIXVfNWEaze0VoUQCZdMPs+pa8Y1PHl8l8mPw6ybaZc1pcpBJLgrmbyQHBvEVJb2gA/mRE/JkSJYcKQgUYmaQ7M5a+Q74jywBrWz9oLUgG5MQmlbsuXUaVBjKe0Frqe+u6tLkj4IjOXhyvTU8uMXBsyW1QVMcFBxew3mAwGrAGrADHXtFshFpk9ytSk3TeQoAEAkNXAMYqBe37Yw/zc0cEkJw/IBCR2xbFB/rlpbTvpg8KtHCf4ecy5TJq+bGzVh92h7Mz7KoFYvIJICgXBzo1cH5RCqUzjLTwPjjnCtqt08kGYubMao7yYpbHVlE8IxIcr3gG4YjEtHvxmWv2ee6+AlLuWAGpDY4fpGi+JHVz4Hjj4wopLFhkSMcRrGywmgCQeeRHHFo1pnZCYOjchnDWfIZjiS1NfTNDm7fc7tKtTlXXCNLQmjv1Lt/YVgEZIOJF5mpUuNfOCagEndavhrGQCDjkSatnzrg0ZM0lgQWLcqc+UNJs2CcWzp4xtMT7LuWHNgXI6frChlliqjZHllSNVByd5wzORiOuDRRpMst2pGmWvwjKkANew0y6RuVG6M6vXy+EYQoe6Klndm5cnMBvLQGJZvWnzgWkgN8vhpjCkmcQ4fE4+sYykEnTTICjwQkpRDPnmOBwrCkpV4gHD0DWElzcAwAp4Plx/SHN6jAPqXwyr6yih9sJJFolsxPeJFOcWvYx9qjhMTze8P79YqzsvL/zMoFmvgtjk7jIYRaFhQ81Ay7xIbjeH74RUXBejMJ/V0af8jGYy0qe1J3lZuo+Lxxnbk78oucFg+KWjtJtVECu9j/FSON7fIYSn1Ul+DCg86xUcepDU54DHi/SEihlCvOox0hdRdgGJPhh/aEkIq5OBxzwoTlEVqhq6838IwlAx4P04tDrug4Yjp89ISmJDk5aYjxgNJbNXGlWw054NWMB2b154QoTmzfP0RBLdT+zjWrGpA/WACkAPQkN6y1hssjpr5V6PDgAFy+FG5dOcddscbOVIQJncXiN9SphTNSqr3UPX3brCucc88+LWM25bZ9hnTzdki+Ri7NWjOqFLTs+bKUQtO9wDpOYIIoH/eJSwTjISm0zJJEpYKZYSoJILVJBBfWuYEdMi1TBKlol2R2CboCgUhgpSbwKXUQCVZJoaUIiXOy/wvHavEKKGWxOZf5+PlGwKiQE+9g2poPj0jr9rybROAQLOJTLJWpV0tewBATjQGgoGaHFntZSUy1SQlUpAukhLEEAAMEF1LLN+ZqOYvPpOLibVZpkk3VpIUADXixYHq8LSdiWmalKkS3dWbAVpnhUM5pSOonW9UxRkosxExQLPcIAWXvkjNikgnBqCsN7KichPdmVPm7ylFaKJmJqElT1IBvEA4uYc7r0unITAQsBSagnE46wkTllyqDHeJ2kpQSV2RZDEOlMurF88rgZgwzaNLemdNQhKbIsd26lJWhIBBfecEFRarUavRM+zi4VKyFDJtOgOOtY1UtRJIJeunm8d3sbYNgmSL6wtlglSzNQnuyC1UqqwSArOOFUpiq6QoXmvKzTl5CLjnMrZ9M2WNJjA0wPCo9O8ZsiCVNqGPP03jBLSTgxrgY2lKVS7kM8WHxjoyWUkMQWy/tTlCYQAAS7MG65/GkKK4CjYOGJy+cI3SSxBDv6HhALLCTRq60DRlaw7PXhTHLwhIJFXvBm8+eFBGy1FSn5EN0Azo0BM9lkj6zJfIq50QouPOLO2Yftpeneo4e8OumMVp2NS9rlmhuJW+GhSfjFk7N/1pdffR1F4eniouWCB4Iyu1Qr5mhOX4qRBdpdl/WUpSlSQUF64VGHw8I7G09n7SLzIepYggO+eMMZmwLTnKJP5k+OMUVvO7LTwdwJVQMyhU9W0hlO2RaEjekrIrhXTN4s5XZ20O5lf0+GMbf4LavuKHVPTPygKkMhSTvBQJyIY9dI0vOCzGuuTGj+cW7M2Vac5b/y+NDCK+zile1ZUKDMaJ9PEFSrAvNlxyGGMNZk8OweofSvDyPSLTtnYpC3azKS9aK/flDIfR0HKgJqcKG6oUODN84KrvvUgqDMcAQSQ+bvweG02Y+VB0x+HKO8tPYK0D2XbRSAG5EK+XWGA7HEs84iv3Djo5ibi/6RQ7QzbspMxKZkuUzJKQkqpd9sBzRql8IdT+2VtJLKlhJ90ITgDQOzkc4mbL2FQsFpkwh2VdSnIPnlXxiQlfR2hXt98pvygYHhE4YnKuSHbG2AD7RHPukk4XWchyWpCUjtRabypgKL11IDoBokuKEMKkl8Y7uV2AkhnkzixPv8hg4GULJ7FSgCPqpP5lEhvHlDhibqvJXai0iYZhEsm6B7AugAgigo7gVhZXbC0LIvXKMwAZmukUzG7hxIo8WJK7KS0gAWJLO+CPm8KS+z0wYWdA/hSPhlhDhj9G6ruT2std0/6eDABApQhxjkfOF0dq7coi7KSplOyZaq0DOXcswLcBk0WGdlTgS0thwu/qIaW2TOli8pOHEU0wrw/WJ+PH6OVVYrZFomKvmQt8fYKX8Ycyuy1sUzSFYOxYfMeUd+sTVNuqIoXvH4U4Q9stltK0uElsParTrGtxNVX9k7F2xg6UpxxWKHL2X0h1K7Ezx7S5aThQ54gR36dj2j7pyzDfGNBsO0O4livFPjjFRxEvsMfftKQAahKK+JPAZQ+T2PsgSHmLLaNXl6pHW/4BaDW6HLO6h8qRqOz9pwuJ/mHTP00BzMvspY8SFmpzUHxheVsWxp/wBk9SS2tSS8dIjs9aMLif5kxv8A9OWr7qP5hF2iCk2aRLLokpSTnn1phEts0/aIP40/1f2hyjspacwj+aJDZ/Za0haFG6wUk+1gAXOUUWJe9UgjRxGIyrdcl8oQXZeHnD9oGiKjFWIaHxhM7PGhiXaBoCFVs4aGNDs7gqJ1oGgIA7P4KjRWzvzR0TQNAcdtDZ26TvGhinV7VOGHl4jLnHpJaAQQcDSID/oqwf8Alk+K/wD9cBGbjtrG6cV2IsRmye8dVVKdmODDLOOslbMGqvKOg2ds6VIQJcpAQgEkJD544w5aNRm9ucTs0fi8o2GzR+KOiaBoo54bMH4oDs0fijoGgaCac2rZY0VENt/YjylMlZof2jvWgKYK80nayhmKD4cW+EWV2Ckqn2YTCCXWsUoMaMMqNSOvHY2wD/wsvz4ceAiU2fs6VIRclS0oQ5N1NA5xMYmOq1lluIhGyR91XjCqdlj7p8Ym2gYRtlDDZg+6fGNhs0fd84l2gaAihs4fdjI2ePujxiUaBoCOTY+AhxLkNpDloGhsI93BCzQRBmCCMwGIIIIAggjMBiCMxiAIIzGIAggggCCMwQGIIIzAYggggCCCMwGIIIIAgjMEBiCCCAIIzGIAgjMEAQQQQBBBBAEEEEAQQQQBBBBAEEEEAQQQQBBBBAEEEEAQQQQBBBBAEEEEAQQQQBBBBAEEEEB//9k=', '75.00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

DROP TABLE IF EXISTS `liste`;
CREATE TABLE IF NOT EXISTS `liste` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expiration` date DEFAULT NULL,
  `tokenConsult` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tokenModif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `liste`
--

INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `tokenConsult`, `tokenModif`) VALUES
(24, 2, 'Soirée fin d&#39;année', 'Organisation d&#39;une soirée avec toute la classe, pour fêter la fin d&#39;année.', '2019-05-26', '5ce18e9fd56d6', '5ce18e9fd56d8'),
(25, 2, 'Baptême Nicolas', 'On organise un baptême pour le petit Nicolas.', '2019-08-23', '5ce19141c1737', '5ce19141c1738'),
(26, 2, 'Repas de Noël', 'Organisation du repas de noël pour 2020.', '2019-12-24', '5ce19324948e8', '5ce19324948ea'),
(27, 2, 'Enterrement Claude', 'Nous enterrons notre grand-père Claude.', '2019-06-06', '5ce194e82b771', '5ce194e82b773');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id_resa` int(5) NOT NULL AUTO_INCREMENT,
  `nom_user` varchar(30) NOT NULL,
  `id_item` int(11) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_resa`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo_utilisateur` varchar(20) NOT NULL,
  `mdp_utilisateur` varchar(20) NOT NULL,
  `statut_utilisateur` int(2) NOT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `pseudo_utilisateur`, `mdp_utilisateur`, `statut_utilisateur`) VALUES
(1, 'admin', '123456', 1),
(2, 'allan', '1234', 2),
(0, 'user_externe', 'visiteur', 0),
(9, 'kevin', 'azer', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
